
#Welcome

sdn-ucp is an Unified Control Platform for Software-Defined Networking (SDN) Northbound API. The current state of these APIs vary from vendor to vendor. This is an attempt to abstract the vendor specific APIs.

#sdn-ucp features
To Support the following northbound api:

    Unified RESTful API
    SNMP Notifications
    Syslog logging 

# What sort of controllers does the sdn-ucp address?
Most of the controllers have their own APIs( REST, XML-RPC , proprietery APIs). I will begin with mapping REST, then followed by XML-RPC. In the first version I will focus on floodlight controller/NOX Controller.

# What sort of applications does the sdn-ucp address?
SNMP and Syslog is mainly for integrating SDN into existing Network Management Systems ( Nagios, OpenNMS, Openview, Tivoli..)
Unified Restful API can be used by any application requiring access into SDN Controllers.

Don't expect a mapping for all of the APIs, simply because the platforms are built upon different architectures and user interfaces. For this first round I'm focused on identifying the abstract mapping when it exists. In the following versions I'll expand the scope and anytime the concepts are similar enough, I'll do my best to provide the appropriate guidance.

# Give it a try!
TBD

# What's next?
Of course, this is a work in progress, coverage will expand and more Controllers will be mapped soon. So, please consider using the sdb-ucp in your application/monitoring efforts, and provide feedback on Issues forum, where you can also suggest new Controller APIs to include

